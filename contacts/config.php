<?php
define ('DB_TYPE', 'mysql');
define ('USER_NAME', 'root');
define ('HOST_NAME', 'localhost');
define ('DB_NAME', 'cfw_test1');
define ('PASS', 'admin123');

define ('DB_PREFIX', '');
define ('ASC', 1);
define ('DESC', 0);
define ('DEFAULT_CONTROLLER', 'contacts');
define ('DEFAULT_ACTION', 'index');
define ('DISPATCH_CONTROLLER', 'default');
define ('DISPATCH_ACTION', 'dispatch');


define ('DIR_TEMPLATES', 'templates' . DS);
define ('DIR_MODELS', 'models' . DS);
define ('DIR_CONTROLLERS',  'controllers' . DS);
define ('DIR_VIEWS',  'views' . DS);
