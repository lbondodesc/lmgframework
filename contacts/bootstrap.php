<?php
ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT);
define ('DS', DIRECTORY_SEPARATOR);
define ('PATH_APP', dirname (__FILE__) . DS);
$pathLibrary =  '..' . DS . 'Cfw'. DS;

define ('PATH_INCLUDES', $pathLibrary); 
//define ('PATH_TEMPLATES' , PATH_INCLUDES . 'templates' . DS);
define ('PATH_TEMPLATES' , PATH_APP . 'includes' . DS  . 'templates' . DS);

define ('PATH_ROOT', getcwd() . DS); 


require_once PATH_APP . 'config.php';
require_once PATH_APP . 'functions.php';
require_once PATH_INCLUDES . 'db.php';
require_once PATH_INCLUDES . 'registry.php';
require_once PATH_INCLUDES . 'front.php';
//require_once PATH_INCLUDES . 'debug.php';
require_once PATH_INCLUDES . 'mvc.php';
require_once PATH_INCLUDES . 'model.php';
require_once PATH_INCLUDES . 'controller.php';
require_once PATH_INCLUDES . 'view.php';
require_once PATH_INCLUDES . 'request.php';
require_once PATH_INCLUDES . 'recordset.php';



$host = $_SERVER['HTTP_HOST']; /// "cfw:8080"
$server_root= $_SERVER['DOCUMENT_ROOT']; 
$res = substr(PATH_ROOT, strlen($server_root), strlen(PATH_ROOT));
$res = str_replace(DS, '/', $res);
$final = ""; 
	if ($_SERVER['SERVER_PORT'] == 443){
		$final = $final . 'https';	
	}else {
		$final = $final . 'http';
	}

$final = $final . '://' . $host . $res;

define ('URL_BASE', $final);

$front = Cfw_Front::getInstance();
$arr = array ('Cfw_Front', 'autoload');
spl_autoload_register($arr);




