<?php
class Cfw_Controller_Users extends Cfw_Controller
{
	private $_limitCount = 10;
	
	// Move to other controller - create new controller for users
	public function actionRegister()
	{
  		$this->render ('register');
	}
	
	public function actionLogin()
	{
		$login = $this->getRequest()->get('auth_name');
		$password = $this->getRequest()->get('auth_pass');
		
		$isValidUser = $this->getFront()->getModel($this->getName())->isValidUser($login, $password);
		if ($isValidUser) {
			session_start();
			//$_SESSION['user_id'] = $row['id'];
			$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
			dump($_SESSION); exit;
		}
	}
	
	public function actionShowLoginForm() 
	{
		if (isset($_REQUEST[session_name()])) session_start();
		if (isset($_SESSION['user_id']) AND $_SESSION['ip'] == $_SERVER['REMOTE_ADDR']) return;
		else
		$this->render ('login');
	}
	public function actionLogout()
	{
		session_start();
		session_destroy();
		$this->redirect('index');
	}

}