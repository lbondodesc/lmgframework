<?php
class Cfw_Model_Users extends Cfw_Model
{
	protected $_id = 'id';
	protected $_table = 'users';
	
	///////////methods////////////////
	
	public function isValidUser($login, $password)
	{
		$user = $this->getRow($this->_table, $login)->current();
		if (!empty($password) && $password === $user['password']) {
			return $user['id'];
		}
		return false;
	}
}