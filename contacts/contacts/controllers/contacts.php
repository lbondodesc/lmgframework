<?php
class Cfw_Controller_Contacts extends Cfw_Controller
{
	private $_limitCount = 10;
	
	public function actionAjaxShow() {
		$id = $this->getRequest()->getId();
		$data = $this->getFront()->getModel($this->getName())->getRow($this->getName(), $id)->current();
		//header('Content-Type: application/json');

		return json_encode($data);
	}
}