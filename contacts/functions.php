<?php
class Cfw_Debug
{
	const DEBUG_MODE = 1;

public static function  timer($timer='default')
{
	static $timers = array();
	if (!self::DEBUG_MODE){
		return;}
	/////work of timer
	$time = microtime(1);
	echo '<PRE style="border: 3px solid #050; background-color: #fdd;">';
	echo "Timer " . '<SPAN style="color: #00f;">' . $timer . "</SPAN>" . " was started!";
	if (isset($timers[$timer])){
	echo sprintf("Timers different: %01.6f", $time - $timers[$timer] );
	}
	echo "</PRE>";
	$timers[$timer] = $time; 
}

public static function dump()
{
	
	$arr_debug = debug_backtrace();
	if (!self::DEBUG_MODE)
	{return;}
	$args = func_get_args();
	echo '<pre style="border: 3px solid #050;
		background-color: #bfb;" >';
	
	foreach ($args as $key=>$value)
	{
		if (is_object($value))
		{
			echo 'Class is ' . '<SPAN style="color: #00f;">' . get_class($value) . '</SPAN>';
			echo '<SPAN style="color: #00f;">' . $arr_debug[2]['file'] . '</SPAN>' . ' (' . $arr_debug[2]['line'] .')';
		}
		else if (is_array($value))
		{
			print_r($value);
			echo '<SPAN style="color: #00f;">' .$arr_debug[2]['file'] . '</SPAN>' . ' (' . $arr_debug[2]['line'] .')';
		}
		else 
		{
		var_dump($value);
		echo '<SPAN style="color: #00f;">' . $arr_debug[2]['file'] . '</SPAN>' . ' (' . $arr_debug[2]['line'] .')';
		}
	}
	echo '</pre>';
}

public static function   backTrace()
{
	if (!self::DEBUG_MODE)
	{return;}
	$arr_debug = debug_backtrace();
	
	echo '<pre style="border: 3px double #050; background-color: #fd5;" >';
	foreach ($arr_debug as $key=>$value)
	{
		if ($key == 0 || $key==1)
		{
			continue;
		}

		$cur_dir = getcwd();

		if (array_key_exists('object', $value))
		{	
			echo 'Class is ' . '<SPAN style="color: #00f;"><b>' . $value['class'] ."</b></SPAN>".
			"| Point of call object: " . substr($value['file'],strlen($cur_dir))  . 
			" (" . $value['line'] . ") \n";

			
		}
		else 
		{
			echo "Function is " . '<SPAN style="color: #00f;"> <b>' . $value['function'] ."</b></SPAN>". 
			"| Point of call function: " . substr($value['file'],strlen($cur_dir)) 
			. " (" . $value['line'] . ") \n";
		
		}
		
	}
	echo "</pre>";
}

private function show()
{
	
}

}

function dump()
{
	$args = func_get_args();
	call_user_func_array(Array('Cfw_Debug', 'dump'), $args);
}
function backTrace()
{
	
	Cfw_Debug::backTrace();
}
function timer($timer = 'default')
{
	Cfw_Debug::timer($timer);
}
function getUrl($controller = NULL, $action = NULL, $id = 0)
{
	$params = array();
	if (! empty ($controller) && is_string($controller)){
  		$params['controller'] = $controller;
  	}
	if (! empty($action) && is_string($action)){
		$params['action'] = $action;
	}
	if (! empty($id) && is_int($id)){
		$params['id'] = $id;
	}
  	if (! empty ($params)){
  		return URL_BASE . '?' . http_build_query($params);
  	}
	return URL_BASE;
}

function getUpperCasedFormat($str)
{
	$str = str_replace ('_', ' ', $str);
	$str = ucwords($str);
	$str = str_replace (' ', '', $str);
	return $str;
}

function getUnderscoresSeparatedFormat($str)
{
	$str = preg_replace('/([A-Z])/', '_$1', $str);
	$str = substr($str, 1);
	$str = strtolower($str);
	return $str;
}
