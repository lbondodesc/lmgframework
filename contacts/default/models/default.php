<?php
class Cfw_Model_Default extends Cfw_Model
{
	function getModel($model) {
		$front = $this->getFront();
		$model =  $front->getModel($model);
		return $model;
	}
}