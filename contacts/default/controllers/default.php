<?php
class Cfw_Controller_Default extends Cfw_Controller
{
	public function actionIndex ()
	{
		return $this->render();
	}
	
	public function actionDispatch()
	{
		$front = $this->getFront();
		$model =  $front->getModel ($front->getController());
		ob_start();
	   	$model->action($front->getAction()); 
	    $content = ob_get_clean();
	    $this->assign ('content', $content);
		return $this->render('master');
	}
}