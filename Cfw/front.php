<?php
class Cfw_Front
{
	private $_action;
	private $_controller;
	private static $_instance;
	private $_models = array();
	private $_controllers = array();
	private $_views = array();
    
	private function __construct()
    {
    }
    public static function getInstance()
    {
        if (! isset(self::$_instance)) {
            $className = __CLASS__;
            self::$_instance = new $className;  
            self::$_instance->_init();
        }
        return self::$_instance;
    }

    public function getRequest()
    {
    	if (! $this->getRegistry()->get('request')){
    		$request = new Cfw_Request(); 
    		$this->getRegistry()->set('request', $request);	
    	}
    	return $this->getRegistry()->get('request');	
    }
    private function _init()
    {
    	$files = glob(PATH_APP . '*' . DS . DIR_MODELS . '*.php');
    	foreach ($files as $path){
    		$name = basename($path, '.php');
    		$this->_models[$name] = $path;
    	}
    	$files = glob(PATH_APP . '*' . DS . DIR_CONTROLLERS . '*.php');
    	foreach ($files as $path){
    		$name = basename($path, '.php');
    		$this->_controllers[$name] = $path;
    	}
   		$files = glob(PATH_APP . '*' . DS . DIR_VIEWS . '*.php');
    	foreach ($files as $path){
    		$name = basename($path, '.php');
    		$this->_views[$name] = $path;
    	}
    	
    	if (! empty($_REQUEST['controller']) && is_string($_REQUEST['controller'])){
	    		if (preg_match("/^\w+$/", $_REQUEST['controller'])){ 
	    			$this->_controller = $_REQUEST['controller'];
	    		}
	    	} 
	    	else{
	    			$this->_controller = DEFAULT_CONTROLLER; 
	    	}
	    
	    if (! empty($_REQUEST['action']) && is_string($_REQUEST['action'])){
	    		if (preg_match("/^\w+$/", $_REQUEST['action'])){ 
	    			$this->_action = $_REQUEST['action'];
	    		}
	    	} 
	    	else {
	    		$this->_action = DEFAULT_ACTION;
	    	}
    }
    
    public function dispatch()
    {
    	//ob_start();
	    echo $this->action(DISPATCH_CONTROLLER, DISPATCH_ACTION);
	   // $content = ob_get_clean();
	    $path = PATH_APP . 'default' . DS . 'templates' . DS . 'default' . DS . 'master.phtml';
	    if (file_exists($path)){
	    	include $path;
	    }
    }
    
    public function action ($controller, $action)
    {
 
	    $registry = $this->getRegistry();
	    $method = 'action' . getUpperCasedFormat($action);
	    
	    $controllerClass = 'Cfw_Controller_'.getUpperCasedFormat($controller);

	    $controllerObj = new $controllerClass();
	    $registry->setController ($controller, $controllerObj);
	    if (method_exists($controllerClass, $method)){
	    	return $controllerObj->$method();
	    } 
	    else {
	    	return $controllerObj->noAction($action);
	    }   	
    }
    
    public function render ($controller, $action)
    {
    	$viewerObj = $this->getView($controller);
	    $viewerObj->render($action);
    }

	public function getView ($viewName)
	{
		$registry = $this->getRegistry();

	    if (! $registry->getView($viewName)){
	    	$viewerClass = 'Cfw_View_' . getUpperCasedFormat($viewName);
			$viewerObj = new $viewerClass();
			$registry->setView ($viewName, $viewerObj);	
	    }
	    return $registry->getView ($viewName);
	}
	
    public function getModel ($modelName)
    {	
    	$registry = $this->getRegistry();
    	if (! $registry->getModel($modelName)){
		    $className = 'Cfw_Model_' . getUpperCasedFormat($modelName); 
		    $modelObj = new $className();
		    $registry->setModel ($modelName, $modelObj);
    	}
    	return $registry->getModel($modelName);
    }
    
    public function getDb ()
    {
    	$registry = $this->getRegistry();
    	if (! $registry->get('db') ){
    		 $db = new Cfw_Db(DB_TYPE);
    		 $registry->set('db', $db);
    	}
    	return $registry->get('db');
    }
  
    public function getRegistry ()
    {
    	return Cfw_Registry::getInstance();
    }
 
   
	public function getUrl ($controller = null, $action = null, $id=0)
	{
		if (empty ($controller)){
			$controller = $this->_controller;
		}  
		if (empty ($action)){
			$action = $this->_action;
		}
		return getUrl($controller, $action, $id);
	}  
	
	public function redirect ($controller = null, $action = null)
	{
		if (empty ($controller)){
			$controller = $this->_controller;
		}  
		if (empty ($action)){
			$action = $this->_action;
		}
		$url = $this->getUrl($controller, $action);
		header('Location: ' . $url);
	}  
	
 	public function getController()
    {
    	return $this->_controller;
    }
    
	public function getAction()
    {
    	return $this->_action;
    }
    public function getViewTemplatesPath($name)
    {
    	if (is_string($name) && isset ($this->_views[$name])){
    		$path = $this->_views[$name]; 
    		
    		//$path = basename(basename($path));
    		$path = dirname(dirname($path));  
    		$path .= DS . DIR_TEMPLATES;
    		return $path;
    	}
    	else{
    		return PATH_TEMPLATES;
    	}
    }
    
     public static function autoload($name)
    {   
    		
    	$type = explode('_', $name);
    	$name = getUnderscoresSeparatedFormat($type[2]);
    	$pathModels = self::$_instance->_models[$name];
    	$pathControllers = self::$_instance->_controllers[$name];
    	$pathViews = self::$_instance->_views[$name];
    	
    	$path="";
    	switch ($type[1]){
	    	case 'Model':  		
	    		$path = $pathModels;
	    		break;
	    	case 'Controller':
	    		$path = $pathControllers;
	    		break;
	    	case 'View': 
	    		$path = $pathViews;
	    		break;
	    		
    	}
    	if (file_exists($path)){
    		require_once $path;
    	}
    }  
}