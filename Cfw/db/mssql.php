<?php

class Cfw_Abstract_Mssql extends Cfw_Abstract
{

	private $_dbResource = NULL;
	private $_queryResource = NULL;

	public function __construct ($hostName, $userName, $pass, $dbName)
	{
		$this->_connect($hostName, $userName, $pass);
		$this->selectDb($dbName);	
	}

	protected function _connect ($hostName, $userName, $pass)
	{
		$this->_dbResource = mssql_pconnect($hostName, $userName, $pass);
		if (! $this->_dbResource){
			Throw new Exception('Can\'t conect to mssql server! ');
		}
	}

	public function selectDb ($dbName)
	{
		$result = mssql_select_db($dbName, $this->_dbResource);
		if (!result){
			Throw new Exception ('Can\'t select mssql data base!');
		}
		return true;
	}

	public function query($sql)
	{
		$this->_queryResource = mssql_query($sql, $this->_dbResource);
		if (!$this->_queryResource){
			Throw new Exception ('Can\'t execute query ' . $sql);
		}
	}

	public function select($sql)
	{
		$res = $this->query($sql);
		$list = array();
		while ($row = mssql_fetch_assoc($res)){
			$list[] = $row;
		}
		return $list;
	}

	public function insert($table, array $date)
	{
		foreach ($date as $key=>$value){
			$date[$key] = $key . '=' . $value; 
			addslashes($date[$key]);
		}	
		$pieces = implode(',', $date);
		$sql = 'INSERT INTO ' . $table . ' SET ' . $pieces;
		$this->query($sql);
		return mssql_rows_affected($this->_dbResource);
	}

	public function update($table,array $date, $where)
	{
		foreach ($date as $key=>$value){
			$date[$key] = $key . '=' . $value;
			addslashes($date[$key]);
		}
		$pieces = implode(',', $date);
		$sql = 'UPDATE ' . $table . ' SET ' . $pieces . ' WHERE ' . $where;
		$this->query($sql);
		return mssql_rows_affected($this->_dbResource);
	}

	public function replace ($table, array $date, $where)
	{
		foreach ($date as $key=>$value){
			$date[$key] = $key . '=' . $value;
			addslashes($date[$key]);
		}
		$pieces = implode(',', $date);
		$sql = 'REPLACE TO ' . $table . ' SET ' . $pieces . ' WHERE ' . $where;
		$this->query($sql);
		return mssql_rows_affected($this->_dbResource);
	}

	public function delete ($table, $where)
	{
		$sql = 'DELETE FROM ' . $table . ' WHERE ' . $where;
		return mssql_rows_affected($this->_dbResource);
	}

	public function getRow($sql)
	{
		$list = $this->select($sql);
		if (! empty($list)){
			return $list[0];
		}
		return array();
	}

	public function getCell ($sql, $nameField)
	{
		$row = getRow($sql);
		if (! empty($row)){
			return $row[$nameField];
		}
		return false;
	}

	public function getList($sql)
	{
		return $this->select($sql);
	}
}