<?php
class Cfw_Abstract_Mysql extends Cfw_Abstract
{
	private $_dbResource;
	private $_resource;
	private $_position;
	
	public function __construct($hostName, $userName, $pass, $dbName)
	{
		$this->_connect($hostName, $userName, $pass);
		$this->selectDb($dbName);
	}
	
   	protected function _connect($hostName, $userName, $pass)
   	{
   		
   		$this->_dbResource = mysql_pconnect($hostName, $userName, $pass);
   		if (! $this->_dbResource){
   			Throw new Exception("Error mysql connect" . mysql_error());
   		}
   	}
   	public function closeConnection ()
   	{
   		if (! mysql_close($this->_dbResource)){
   			Throw new Exception('Can\'t close mysql connection ' . mysql_error());
   		}
   	}
   
   	public function selectDb($dbName)
   	{
   		$result = mysql_select_db($dbName, $this->_dbResource);
   		if (! $result){
   			Throw new Exception ("Can't select mysql db: " . $dbName);	
   
   		}
   		return true;
   	}

   	public function query($sql)
   	{
   		$res = mysql_query($sql, $this->_dbResource);
   		if (! $res){
   			Throw new Exception ("Can't execute query!");		
   		}
   		$this->_resource = $res;
   		return $res; 
   	}

   	public function select($sql)
   	{
   		$this->_resource = $this->query($sql);
   		$numRows = $this->getNumRows($this->_resource);
   		return new Cfw_Recordset($this, $this->_resource, $numRows);
   	}

   	public function insert ($table,array $date)
   	{
   		$date = $this->prepareFields($table, $date);
   		$fieldsName = implode (',', array_keys($date));
   		$fieldsValue = implode ('\' , \'', array_values($date));
		$fieldsValue = '\'' . $fieldsValue . '\'';
		//$sql = stripslashes($fieldsValue);
   		$sql = 'INSERT INTO ' . $table . ' (' . $fieldsName . ') ' . 'VALUES (' . $fieldsValue . ')';
   		
   		$this->query($sql);
   		return mysql_affected_rows($this->_dbResource);
   	}
   
   	public function update ($table, array $date, $where)
   	{
   		$where = $this->prepareWhere($table, $where);
   		foreach ($date as $key=>$value){
   			$date[$key] = sprintf("`%s`='%s'", $key, $value);
   		}
   		$pieces = implode(', ', $date);
   		$sql = 'UPDATE ' . $table . ' SET ' . $pieces . ' WHERE ' . $where;
   		$this->query($sql);
   	   return mysql_affected_rows();
   	}
   
   	public function replace ($table, array $date, $where)
   	{
   		$where = $this->prepareWhere($table, $where);
   		foreach ($date as $key=>$value){
   			$date[$key] = $key . '=' . $value;
   		}
   		$pieces = implode(',', $date);
   		$sql = 'REPLACE INTO ' . $table . ' SET ' . $pieces . ' WHERE ' . $where;
   		
   		$this->query($sql);
   	   return mysql_affected_rows();
   	}

   	public function delete ($table, $where)
   	{
   
   		$where = $this->prepareWhere($table, $where);
   		if (empty($where)) {
   			Throw new Exception ("don't exist where");
   		}
   		$sql = 'DELETE FROM ' . $table . ' WHERE ' . $where ;
   		$this->query($sql);
   		return mysql_affected_rows();
   	}
	
   	public function getRow ($table, $where=null)
   	{
   		if (is_numeric($where) && !empty($where)) {
   			$sql = 'SELECT * FROM `' . $table . '` WHERE `id`=' . $where;
   		} else {
   			$sql = 'SELECT * FROM `' . $table . '`';
   		}
   		$list = $this->select($sql);
   		
   		if (! empty($list)){
   			return $list;
   		}
   		return array();
   	}

   	public function getCell ($sql, $nameField)
   	{
   		$list = $this->getRow($sql);
   		if (! empty($list)){
   			return $list[$nameField];	
   		}	
   		return null;
   	}
   	
   	public function getList($sql)
   	{
		return $this->select($sql);
   	}
   	
 	public function	getFields($table)
   	{
   		$sql = 'DESCRIBE ' . $table;
   		$structure = $this->getList($sql);
   		$fields = array();
   		foreach ($structure as $key=>$value){
   			$fields[$value['Field']] = $value['Field'];
   		}
   		return $fields;
   	}
   	
   	public function prepareFields ($table, array $formFields)
   	{
   		
   		$tableStructure = $this->getFields($table);
   		$fieldsTrue = array();
   		
   		foreach ($formFields as $formField=>$valueForm){
   			foreach ($tableStructure as $number=>$tableField){
   				if ($formField == $tableField){
   					$fieldsTrue[$formField] = $valueForm;
   				}
   			}
   		}
   	if (empty($fieldsTrue)){
   			Throw new Exception ("Array of true fields is empty!");
   		}
   		return $fieldsTrue;
   	}
   
   	public function prepareWhere ($table, $where)
   	{
   		if (is_string($where)){
   			return $where;
   		} 
   		else if (is_array($where)){
   			$fields = $this->prepareFields($table, $where);
   			if (! empty($fields)){
   			$where = implode(' ',$where);
   			 return $where;
   			}
   		}
   		else if (is_int($where)){
   			$where = 'id = ' . $where;
   			return  $where;
   		}
   		
   	}
   	
	public function seek($position = 0)
	{
	    if(mysql_data_seek($this->_resource, $position)){
	    	$this->_position = $position;
	    	return 1;
	    }else{
	    	throw new Exception('seek failed! mysql.php');
	    }
	}
	
	public function next($resource)
	{
		if ($row = mysql_fetch_assoc($resource)){
   			return $row;
		} else {
			throw new Exception('next() failed!');
		}
	}
	public function getNumRows($resource)
	{
		return mysql_num_rows($resource);
	}
}