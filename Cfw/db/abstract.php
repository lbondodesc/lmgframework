<?php

abstract class Cfw_Abstract
{
	
	public function __construct($hostName, $userName, $pass, $dbName)
	{
		$this->_connect($hostName, $userName, $pass);
		$this->select_Db($dbName);
		
	}
	
   protected abstract function _connect($hostName, $userName, $pass);
   
   public abstract function selectDb($dbName);
   
   public abstract function query($sql);
   
   public abstract function select($sql);
   
   public abstract function insert ($table, array $date);
   
   public abstract function update ($table, array $date, $where);
   
   public abstract function replace ($table, array $date, $where);
   
   public abstract function delete ($table, $where);
   
   public abstract function getRow ($table, $where);
   
   public abstract function getCell ($sql, $nameField);
   
   public abstract function getList ($sql);
   
   public abstract  function seek($position = 0);

   public abstract  function next($resource);

   public abstract  function getNumRows($resource);

}