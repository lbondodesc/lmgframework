<?php
abstract class Cfw_Controller extends Cfw_Mvc
{
	private $_limitCount = 10;
	
	public function noAction($action)
	{
		throw new Exception('Action ' . $action . ' not found!');
	}
	
	public function actionIndex()
	{
		$fields = $this->getFront()->getModel($this->getName())->limit(4)->getList();
		//$fields = $this->getFront()->getModel($this->getName())->joinInner('users', 'contacts.id=users.id', array ('users.id'))->getList();		
  		$this->assign('fields', $fields);
  		$this->render ('index');
	}
	
	public function actionDelete()
	{
		 $id = $this->getRequest()->getId();
		 $this->getFront()->getModel($this->getName())->delete($id); 
  		 return $this->redirect('index');
	}
	
	public function actionInsert()
	{
		 $item = $this->getRequest()->getPost('item'); 
		 if (! empty ($item)) {
		 	$this->getFront()->getModel($this->getName())->insert($item); 
		 }
  		 $this->redirect('index');
		  
	}
	
	public function actionUpdate()
	{
		// $this->getFront()->getModel($this->getName())->update($_REQUEST[$this->getName()], intval($_REQUEST['contacts']['id']));
		 $item = $this->getRequest()->getPost('item'); 
		 $id = $this->getRequest()->getId();
		 if (! empty($item)){
		 	$this->getFront()->getModel($this->getName())->update($item, $id);
		 }
  		 $this->redirect('index');
	}
	
	public function actionModify()
	{
		$id = $this->getRequest()->getId();
		$item = $this->getFront()->getModel($this->getName())->getRow($this->getName(), $id);
		dump($item);
		$this->assign('item', $item);
		$this->assign('id', $id);
  		$this->render ('form');
	}
	
	public function  actionShow()
	{
		$id = $this->getRequest()->getId();
		$item = $this->getFront()->getModel($this->getName())->getRow($this->getName(), $id);
		$this->assign('item', $item);
  		$this->render ('show');
	}
	
	public function  actionCreate()
	{
		$this->render('form');
	}
}