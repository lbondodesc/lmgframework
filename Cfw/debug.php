<?php

class Cfw_Debug
{
    const DEBUG_MODE = 0;

    private static function outHead($backtrace, $funcName)
    {
        if ( $backtrace[1]['function'] == 'call_user_func_array' ) {
            $headBacktrace = $backtrace[2];
            array_shift($backtrace);
            array_shift($backtrace);
        } else {
            $headBacktrace = $backtrace[0];
        }

        $relativePath = str_replace(getcwd(), '.', $headBacktrace['file']);

        echo '<div style="padding: 0 0 4px">'
           , '<span style="color: #00d">Function:</span> '
           , '<span style="color: #000">' , $funcName , '</span> '
           , '<span style="color: #00d">File:</span> '
           , '<span style="color: #000">' , $relativePath , '</span> '
           , '<span style="color: #00d">Line:</span> '
           , '<span style="color: #f00">' , $headBacktrace['line'] , '</span>'
           , '</div>';
    }

    public static function backtrace()
    {
        $backtrace = debug_backtrace();

        echo '<pre style="border-radius: 8px; border: 3px double #090; background-color: #dfd; padding: 5px; margin: 2px 0;">';

        self::outHead($backtrace, 'Backtrace()');

        array_shift($backtrace);

        foreach ($backtrace as $var) {
            //echo '<br />';
            echo '<div style="border: 1px solid #090; margin: 3px 0; background-color: #efe;">'
               , '<span style="border: 1px solid #090; background-color: #fff; margin: 2px 0px; padding: 2px; ">'
               , '<span style="color: #00d">File:</span> '
               , '<span style="color: #000">' , $var['file'] , '</span> '
               , '<span style="color: #00d">Line:</span> '
               , '<span style="color: #f00">' ,$var['line'] . "</span></span>\n";
           if ( isset($var['object']) ) {
               echo '<span>' , $var['class'] , '</span>'
                  , '<span>' , $var['type']  , '</span>';
               //$var['object'] = 'Type is object';
           }
           $strArgs = '';
           foreach ($var['args'] as $argument) {
               if ((gettype($argument) == 'object') || (gettype($argument) == 'object'))
                   $strArgs .= gettype($argument);
               else
                   $strArgs .= $argument;
           }
           echo '<span>' , $var['function']
              , '(' , $strArgs , ')'
              , '</span>'
              , "\n";
           //print_r($var);
           echo '</div>';
        }
        echo '</pre>';
    }

    public static function dump()
    {
        $args = func_get_args();
        $backtrace = debug_backtrace();

        echo '<pre style="border-radius: 8px; border: 3px double #900; background-color: #fdd; padding: 5px; margin: 2px 0;">';

        self::outHead($backtrace,'Dump()');

        foreach ($args as $argument) {
            echo '<div style="border: 1px solid #900; margin:2px 0; background: #fee">';
            var_dump($argument);
            echo '</div>';
        }

        echo '</pre>';
    }

    public static function timer($timer = 'default')
    {
        static $timersList = array();

        $backtrace = debug_backtrace();

        echo '<pre style="border-radius: 8px; border: 3px double #fc0; background-color: #ffd; padding: 5px; margin: 2px 0;">';

        self::outHead($backtrace,'Timer()');

        echo '<div style="border: 1px solid #fc0; background-color: #fff; padding: 2px;">';
        $currMicrotime = microtime(1);
        if (!isset($timersList[$timer])) {
            $timersList[$timer] = $currMicrotime;
            echo 'Timer: "' , $timer , '" start';
        } else {
            $timersList[$timer] = $currMicrotime - $timersList[$timer];
            echo 'Timer: "' , $timer , '" stop ' , sprintf("%.6f",$timersList[$timer]);
        }
        echo '</div>';
        echo '</pre>';
    }
}


function dump()
{
    $args = func_get_args();
    call_user_func_array('Cfw_Debug::dump', $args);
}

function backtrace()
{
    $args = func_get_args();
    call_user_func_array('Cfw_Debug::backtrace', $args);
}

function timer($timer = 'default')
{
    $args = func_get_args();
    call_user_func_array('Cfw_Debug::timer', $args);
}
