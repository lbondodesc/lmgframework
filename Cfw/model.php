<?php
abstract class Cfw_Model extends Cfw_Mvc
{
	protected $_id;
	protected $_sqlData = array();
	private $_totalCount = 0;
	

	public function getDb()
	{
		return $this->getFront()->getDb();
	}
	
	public function insert (array $date)
   {
   		return $this->getDb()->insert ($this->getTable(), $date);
   }
  
   public function update (array $date, $where)
   {
   		return $this->getDb()->update($this->getTable(), $date, $where);
   }

   public function replace (array $date, $where)
   {
   		return $this->getDb()->replace ($this->getTable(), $date, $where);
   }
   public function __toString()
   {
   		return $this->getTable();
   }

   public function delete ($where)
   {
   		return $this->getDb()->delete ($this->getTable(), $where);
   }
   
   public function getRow ($table, $where)
   {
   	$this->_sqlData = array();
   	return $this->getDb()->getRow($table, $where);
   }
   
   public function getCell ($sql, $nameField)
   {
   		$this->_sqlData = array();
   		return $this->getDb()->getCell($sql, $nameField);
   }
   
   public function getList()
   {
  		 if (!empty($this->_sqlData['join'])){
   			foreach ($this->_sqlData['join'] as $type=>$tables ){
   				foreach ($tables as $table=>$data){
   					$this->fields($this->_sqlData['fields'], $table);
   				}
   			}
  		 }
   		
  		$allFields = array();
   		if (! empty ($this->_sqlData['fields'])){
   			foreach ($this->_sqlData['fields'] as $table => $fields){
   				$allFields += $this->getDb()->prepareFields($table, $fields);
   			}
   		}
   		
   		$sql = 'SELECT ' . ' SQL_CALC_FOUND_ROWS ' 
   				. (!(empty($allFields)) ? implode(', ', $allFields) : implode(', ', $this->getFields()))
   				. ' FROM ' . $this->getTable();
   		
  		 if (!empty($this->_sqlData['join'])){
   			foreach ($this->_sqlData['join'] as $type=>$tables ){
   				foreach ($tables as $table=>$data){
   					$this->fields ($this->_sqlData['fields'], $table);
   					$sql.=' ' . $type . ' JOIN ' . $table . ' ON ' . $this->_sqlData['condition'];
   				}
   			}
  		 }
  		 
  		 $where = array();
  		 if (! empty($this->_sqlData['where'])){
  		 	foreach ($this->_sqlData['where'] as $k => $v){
  		 		if (is_int($k)){
  		 			$where[] = $v;
  		 		} else{
  		 			$where[] = $k . ' ' . (count($where)>1) ? ' ' . 'in(`' . (implode('`,`', $where)) . '`)' : '=`' . $where[0] . '`';
  		 		}
  		 	}
  		 }
  		 if (! empty($where)){
  		 	$sql .= ' WHERE ' .  implode(' AND ', $where);
  		 }

  		 $order = array();
  		 if (! empty($this->_sqlData['order'])){
  		 	foreach ($this->_sqlData['order'] as $field => $direction){
  		 		$order[] = $field . ' ' . (($direction == ASC) ? 'ASC' : 'DESC');
  		 	}
  		 }
  		
  		 if (! empty ($order)){
  		 	$sql .= ' ORDER BY ' . implode (',', $order);
  		 }
  		 
		 if (! empty($this->_sqlData['limit'])){
   			$sql .= ' LIMIT ' . $this->_sqlData['limit']['offset'];
   		}
		$this->_sqlData = array();
  		$result = $this->getDb()->getList($sql);
  		$this->_totalCount = $this->getDb()->getLastQueryTotalCount();
  		return $result;
   }
 
   public function getTotalCount ()
   {
   		return $this->_totalCount;
   }
   
   public function getFields ()
   {
	   	static $fields;
	   	if (is_null($fields)){
	   		$fields = $this->getDb()->getFields($this->_table);
	   	}
	   	$table = $this->getTable();
	   	foreach ($fields as $k => $v){
	   		$fields[$k] = $table . '.' . $v;
	   	}
	   	return $fields;
   }
   
   
   public function getTable()
   {
   		return DB_PREFIX . $this->getName();
   }
   
   public function getId()
   {
   		return $this->_id;
   }
   
   public function fields($table = NULL, array $fields)
   {
   	if (is_null($table)){
   		$table = $this->getTable();
   	}
   	if (isset($this->_sqlData['fields'][$table])){
   		$this->_sqlData['fields'][$table] = array_merge($fields, $this->_sqlData['fields'][$table]);
   	} else {
   		$this->_sqlData['fields'][$table] = $fields;
   	}
   	$this->_sqlData['fields'][$table] = array_unique ($this->_sqlData['fields'][$table]);
   }
   
	public function where($where1, $where2 = NULL)
    {
    	if (func_num_args()>1){
    		if (is_string($where1)){
   			$this->_sqlData['where'][$where1][][$where2] = $where1;
    		}
    		else{
    			 throw new Exception('Bad parameter ' . $where1);
    		}
    	} else{
    		if (is_array($where1)){
    			foreach ($where1 as $k => $v){
    				$this->where($k, $v);
    			}
    		} else if (is_string ($where1)){
    				$this->_sqlData['where'][] = $where1;
    			} else if (is_int($where1)){
    				$this->getId();
    			}
    		}
    	return $this;
    }
    

	public function order($field, $direction = ASC)
    {
   		$this->_sqlData['order'][$field] = $direction;
   		return $this;
    }
    public function limit($offset = 0, $count = 999999)
    {
    	$this->_sqlData['limit'] = array ('offset' => $offset, 'count' => $count);
    	return $this;
    }
	public function key($key)
    {
   		$this->_sqlData['key'] = $key;
   		return $this;
    }
    
	public function join($type, $table, $condition, array $fields = array())
    {
    	$table = "$table";
    	if (in_array(strtolower($type), array('left', 'right', 'inner')) && is_string($condition)){
    		$this->_sqlData['join'][$type][$table] = array('fields' => $fields, 
   														   'condition' => $condition);
   			return $this;
    	} else {
    		throw new Exception ('Bad parameter passed into join!');
    	}
    }
   		
    
	public function joinInner($table, $condition, array $fields = array())
    {
   		return $this->join ('inner', $table, $condition, $fields);
    }
    
	public function joinLeft($table, $condition, array $fields = array())
    {
   		return $this->join ('left', $table, $condition, $fields);
    }
    
	public function joinRight($table, $condition, array $fields = array())
    {
   		return $this->join ('right', $table, $condition, $fields);
    }
    
    public function prepareWhere ($table, $where)
    {
    	$this->getFront()->getDb()->prepareWhere ($this->getTable(), $where);
    }
    
    public function prepareFields (array $fields)	
    {
    	$this->getFront()->getDb()->prepareFields ($this->getTable(), $fields);
    }
    	
}