<?php
class Cfw_Recordset implements Iterator, Countable, ArrayAccess
{
	private $_position;
	private $_resource;
	private $_adapter;
	private $_numRows;
	
    public function __construct($adapter, $resource, $numRows) 
    {
        $this->_adapter = $adapter;
        $this->_resource = $resource;
        $this->_position = 0;   
        $this->_numRows = $numRows;
    }

    function rewind() 
    {
        $this->_position = 0;
        $this->_adapter->seek(0);
    }

    public function current() 
    {
    	$front = Cfw_Front::getInstance();
    	$row = $this->_adapter->next($this->_resource);
    	$front->getModel($front->getController())->assign('item', $row);
        return $row;
    }

    public function key() 
    {
        return $this->_position;
    }

    public function next() 
    {
        ++$this->_position;
    }

    public function valid() 
    {
    	//$numRows = $this->_adapter->getNumRows ($this->_resource);
    	return ($this->_position < $this->_numRows);
    }
    public function count()
    {
    	return $this->_numRows;
    }
    
    public function offsetSet($offset, $value) 
    {   
    }
    
    public function offsetExists($offset) 
    {
    	$offset = intval($offset);
    	return ($offset >= 0 && ($offset < $this->_numRows));
    }
    
    public function offsetUnset($offset) 
    {
    }
    
    public function offsetGet($offset) 
    {
    	$row = $this->current();
    	$offset = intval($offset);
    	$this->_position = $offset;
    	return $this->_adapter->seek($offset);
    }
}