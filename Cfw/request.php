<?php
class Cfw_Request
{

	protected function getFront()
	{
		$front = Cfw_Front::getInstance();
		return $front;
	}	
	
	public function getController()
	{
		return $this->getFront()->getController();
	}
	
	public function __call($name, $args) 
    {
    	
    	$key = getUnderscoresSeparatedFormat(substr($name, 3, strlen($name)));
	    if (strpos($name, 'get') === 0){
	    	
	    		if (empty ($args[0])) $args[0] = NULL;
	        	return $this->get($key, $args[0]);
	    }
	    if (strpos($name, 'set') === 0){
	    		if (empty($args[0])) $args[0] = NULL;
	        	return $this->set($key, $args[0]);
	    }        	
    }
    
	public function setField($key, $value)
    {
    	$controller = $this->getController();
    	if (isset($_REQUEST[$key])){
    		throw new Exception ("Unable to set var! " . $key . ' already exists!');
    	} else {
    		$_REQUEST[$controller]['item'][$key] = $value;
    		return true;
    	}
    }
   
	public function getField($key, $defaultValue = NULL)
    {	
    	$controller = $this->getController();
    	if (isset($_REQUEST[$controller]['item'][$key])){
    		return $_REQUEST[$controller]['item'][$key];
    	}
    	else if (isset($_REQUEST[$key])){
    		return $_REQUEST[$key];
    	} 
    	return $defaultValue;
    }
    
    public function getPost($key, $defaultValue = NULL)
    {
    	$controller = $this->getController();
    	if (isset($_POST[$controller][$key])){
    		return $_POST[$controller][$key];
    	}
    	else if (isset($_POST[$key])){
    		return $_POST[$key];
    	} 
    	return $defaultValue;
    }
    
	public function setPost($key, $value) //$item
    {
    	$controller = $this->getController();
    	if (isset($_POST[$key])){
    		throw new Exception ("Unable to set var! " . $key . ' already exists!');
    	} else {
    		$_POST[$controller][$key] = $value;
    		return true;
    	}
    }
	
    public function set($key, $value) //$item
    {
    	$controller = $this->getController();
    	if (isset($_REQUEST[$key])){
    		throw new Exception ("Unable to set var! " . $key . ' already exists!');
    	} else {
    		$_REQUEST[$controller][$key] = $value;
    		return true;
    	}
    }
   
	public function get($key, $defaultValue = NULL)
    {	
    	$controller = $this->getController();
    	if (isset($_REQUEST[$controller][$key])){
    		return $_REQUEST[$controller][$key];
    	}
    	else if (isset($_REQUEST[$key])){
    		return $_REQUEST[$key];
    	} 
    	return $defaultValue;
    }
    
    public function getId ($defaultValue = 0)
    {
    	return intval($this->get('id')) ;
    }
    
}