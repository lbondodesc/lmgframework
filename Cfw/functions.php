<?php

function getUrl($controller = NULL, $action = NULL, $id = 0)
{
	$params = array();
	if (! empty ($controller) && is_string($controller)){
  		$params['controller'] = $controller;
  	}
	if (! empty($action) && is_string($action)){
		$params['action'] = $action;
	}
	if (! empty($id) && is_int($id)){
		$params['id'] = $id;
	}
  	if (! empty ($params)){
  		return URL_BASE . '?' . http_build_query($params);
  	}
	return URL_BASE;
}

function getUpperCasedFormat($str)
{
	$str = str_replace ('_', ' ', $str);
	$str = ucwords($str);
	$str = str_replace (' ', '', $str);
	return $str;
}

function getUnderscoresSeparatedFormat($str)
{
	$str = preg_replace('/([A-Z])/', '_$1', $str);
	$str = substr($str, 1);
	$str = strtolower($str);
	return $str;
}
