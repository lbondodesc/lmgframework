<?php
class Cfw_Session
{
	
	protected function getFront()
	{
		$front = Cfw_Front::getInstance();
		return $front;
	}	
	
	public function getSession()
	{
		//return $this->getFront()->getController();
	}
	
	public function __call($name, $args) 
    {
    	
    	$key = getUnderscoresSeparatedFormat(substr($name, 3, strlen($name)));
	    if (strpos($name, 'get') === 0){
	    	
	    		if (empty ($args[0])) $args[0] = NULL;
	        	return $this->get($key, $args[0]);
	    }
	    if (strpos($name, 'set') === 0){
	    		if (empty($args[0])) $args[0] = NULL;
	        	return $this->set($key, $args[0]);
	    }        	
    }
    
    public function set($key, $value) 
    {
    	if (isset($_SESSION[$key])){
    		throw new Exception ("Unable to set Session var! " . $key . ' already exists!');
    	} else {
    		$_SESSION[$key] = $value;
    		return true;
    	}
    }
   
	public function get($key, $defaultValue = NULL) 
    {	
    	if (isset($_SESSION[$key])){
    		return $_SESSION[$key];
    	}
    	
    	return $defaultValue;
    }
}