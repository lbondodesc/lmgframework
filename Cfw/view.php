<?php
abstract class Cfw_View extends Cfw_Mvc
{
	public function render ($action = NULL)
	{
		$controller = $this->getName();
		
		$path =  $this->getFront()->getViewTemplatesPath($this->getName()) . $action . '.phtml';
		if (file_exists($path)) {
			$data = $this->getFront()->getRegistry()->get('assign');
				if (isset($data[$controller])){
					extract($data[$controller], EXTR_REFS);
				}
			include $path;
		} else throw new Exception('Template ' . $controller . DS . $action . ' not found!');
	}
	
	public function getValue ($key, $defaultValue = "")
	{
		$controller = $this->getName();
		$data = $this->getFront()->getRegistry()->get('assign');
		
		if (isset($data[$controller]['item'][$key])){
			return $data [$controller]['item'][$key];
		}
		else {
			return $defaultValue;
		}
	}
	
	public function getTextArea ($name)
	{
		
	}
	
	public function getText ($name)
	{
		$controller = $this->getName();
		$textElement = '<input type="text" name="' . $controller .'[item][' . $name . '] " value="' .
		 		 	$this->getValue($name) . '"/>';
			return $textElement;
	}
	
	public function getSelect ($name, array $fields)
	{
		$controller = $this->getName();
		$this->getFront()->getModel();
		$selectElement = '<select name="' . $name . '">';
		$x = 0;
			while ($x < sizeof ($fields)){
			$selectElement .= '<option value="' . $fields[$x] . '">' . $fields[$x];
			$x++;
			}
		$selectElement .= '</select>';
		return $selectElement;
	}
	
	public function getCheckbox()
	{
		
	}
	
	public function getSubmit($name)
	{
		$submitElement = '<input type="submit" name="' . $name .'" value="Send" />';
		return $submitElement;
	}
	
	public function renderForm ($nameForm)
	{
		$controller = $this->getName();
		$data = $this->getFront()->getRegistry()->get('assign');
		$item = $data [$controller]['item'];
		$formElement = '<form action = "' . empty($item) ? getUrl($this->getName(), 'insert') : $this->getUrl($this->getName(), 'update', $this->getId()) 
		        . 'name = "' . $nameForm . '"';
		
		return $formElement;
	}
	public function pagenator ($totalCount = 120, $limitCount = 5, $recordOffset = 12, $pagesLinkCount = 5)
	{
		$totalPages = ceil($totalCount/$limitCount); // 53/10 = 6
		$currentPage = floor($recordOffset/$limitCount); // 12/10 = 1 /// Number of page = 2
		//////StartPage
		$temp = floor ($currentPage/$pagesLinkCount); // 1/5 = 0
		$currentStartPage = $temp * $pagesLinkCount; // 0 * 5 = 0
		/////EndPage
		$currentEndPage = $currentStartPage + $pagesLinkCount; //0+5 = 5
		if ($currentEndPage > $totalPages){
			$currentEndPage = $totalPages;
		}
		/////Make Links///////
		$strPrev = ' Prev ';
		$strNext = ' Next ';
		$strFirst = ' << ';
		$strLast = ' >> ';
		$links = '';
		
		if ($currentPage == 0){
			$links .= $strFirst;
		} else{
			$links = $this->createLink(0, $strFirst);
		}
		
		if($currentPage == 0){
	      $links .= $strPrev;
	    }else{
	      $links .= $this->createLink($currentPage-1, $strPrev);
	    }
	    
	    $links .= $this->formLinks ($currentPage, $currentStartPage, $currentEndPage);
	      
	    if($currentPage == ($totalPages - 1)){
	      $links .= $strNext;      
	    }else{
	      $links .= $this->createLink($currentPage + 1, $strNext);
	    }
	   
	    if($currentPage == ($totalPages - 1)){
	       $links .= $strLast;
	    }else{
	      $links .= $this->createLink($totalPages - 1, $strLast);
	    }
		/*if ($currentPage == $currentEndPage && (($currentEndPage + $limitCount) < $totalPages)){
			$links .= $this->formLinks ($currentPage, $currentEndPage, $currentEndPage + $limitCount);
		}*/ 

		return $links;	
	}
	
	private function formLinks ($currentPage, $currentStartPage, $currentEndPage)
	{
		$links = array();
		for ($i = $currentStartPage; $i < $currentEndPage; $i++){
			if ($i === $currentPage){
				$links[$i] =  $currentPage + 1;
				continue;
			} 
			$links[$i] = $this->createLink($i, $i+1);
		}
		$list =  implode(' ', $links);
		return $list;
	}
	
	private function createLink ($offset, $strDisplay)
	{
		$action = $this->getFront()->getAction();
		$strLink = '<a href="' . $this->getUrl($action). '&page='. $offset .'">';
    	$strLink .= $strDisplay . '</a>';
    	return $strLink;
	}
	
}