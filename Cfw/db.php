<?php
class Cfw_Db
{

	private $_adapter;

	public function __construct($type) {
		
		$fileAbstract = dirname(__FILE__) . DS . 'db' . DS . 'abstract.php';
		$fileChild = dirname(__FILE__) . DS . 'db' . DS . $type . '.php';
		if (! file_exists($fileAbstract)) {
			Throw new Exception('Can\'t include: ' . $fileAbstract);
		}
		if (! file_exists ($fileChild)) {
			Throw new Exception ('Can\'t include: ' . $fileChild);
		}

			require_once $fileAbstract;
			require_once $fileChild;
			$className = 'Cfw_Abstract_' . ucwords($type);
			
			if (! class_exists($className)) {
				Throw new Exception ('Can\'t find class: ' . $className);
			}

			$this->_adapter = new $className(HOST_NAME,USER_NAME, PASS, DB_NAME);
	}

	public function insert ($table, array $date)
   {
   		return $this->_adapter->insert($table, $date);
   }

   public function update ($table, array $date, $where)
   {
   		return $this->_adapter->update($table, $date, $where);
   }

   public function replace ($table, array $date, $where)
   {
   		return $this->_adapter->update($table, $date, $where);
   }
   
   public function delete ($table, $where)
   {
   		return $this->_adapter->delete($table, $where);
   }
   
   public function getRow ($table, $where) {
       return $this->_adapter->getRow($table, $where);
   }
   
   public function getCell ($sql, $nameField)
   {
   		return $this->_adapter->getCell($sql, $nameField);
   }
   public function getList($sql)
   {
   		return $this->_adapter->getList($sql);
   }
   public function getFields ($table)
   {
   		return $this->_adapter->getFields($table);
   }
   public function prepareFields ($table, array $fields)
   {
   		return $this->_adapter->prepareFields ($table, $fields);
   }
   public function prepareWhere ($table, $where)
   {
   		return  $this->_adapter->prepareWhere($table, $where);
   }
   public function getLastQueryTotalCount ()
   {
   		$totalCount = $this->_adapter->select('SELECT FOUND_ROWS() as totalCount', 'totalCount');
   		return $totalCount;
   }
  
}