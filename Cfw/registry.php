<?php
class Cfw_Registry
{
	private static $_instance;
	private  $_data = array();

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (!isset(self::$_instance)) {
            $className = __CLASS__;
            self::$_instance = new $className;
        }
        return self::$_instance;
    }
    
    public function __clone()
    {
        trigger_error('Clone not allowed!', E_USER_ERROR);
    }
    
    public function __call($name, $args) 
    {
    	$key = getUnderscoresSeparatedFormat(substr($name, 3, strlen($name)));
	    if (strpos($name, 'get') === 0){
	    		if (empty ($args[0])) $args[0] = NULL;
	        	return $this->get($key, $args[0]);
	    }
	    if (strpos($name, 'set') === 0){
	    		if (empty($args[0])) $args[0] = NULL;
	        	return $this->set($key, $args[0]);
	    }        	
    }
    
    public function getData ()
    {
    	return $this->_data;
    }
   
    public function set($key, $value)
    {
    	if (isset($this->_data[$key])){
    		throw new Exception ("Unable to set var! " . $key . ' already exists!');
    	} else {
    		$this->_data[$key] = $value;
    		return true;
    	}
    }
   
	public function get($key, $defaultValue = NULL)
    {	
    	if (isset($this->_data[$key])){
    		return $this->_data[$key];
    	}
    	else return $defaultValue;
    }
    
    public function getModel ($key, $defaultValue = NULL)
    {
    	if (isset($this->_data['models'][$key]))
    	{
    		return $this->_data['models'][$key];
    	}
    	else return $defaultValue;
    }
    
	public function setModel ($key, $value)
    {
	   if (isset($this->_data['models'][$key]))
	    	{
	    		throw new Exception ("Unable to set model! " . $key . ' already exists!');
	    	}
	    	$this->_data['models'][$key] = $value;
	    	return true;
    }
    
    public function setModels ()
    {
    	
    }
    
	public function getController ($key, $defaultValue = NULL)
    {
    	if (isset($this->_data['controllers'][$key])){
    		return $this->_data['controllers'][$key];
    	}
    	else return $defaultValue;
    }
    
	public function setController ($key, $value)
    {
	   if (isset($this->_data['controllers'][$key])){
	    		throw new Exception ("Unable to set controller! " . $key . ' already exists!');
	    	}
	    	$this->_data['controllers'][$key] = $value;
	    	return true;
    }
    
    public function setControllers (){}
    
	public function getView($key, $defaultValue=NULL)
    {
    	if (isset($this->_data['views'][$key]))
    	{
    		return $this->_data['views'][$key];
    	}
    	else return $defaultValue;
    }
    
    public function setView($key, $value) 
    {
    	if (isset($this->_data['views'][$key]))
	    	{
	    		throw new Exception ("Unable to set controller! " . $key . ' already exists!');
	    	}
	    	$this->_data['views'][$key] = $value;
	    		return true; 	
    }
    
    public function setViews(){}
    
    public function setTemplateVar($controller ,$key, $value)
    {
    	$this->_data['assign'][$controller][$key] = $value;
    }
    
}
