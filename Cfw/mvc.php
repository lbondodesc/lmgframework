<?php
abstract class Cfw_Mvc
{
	public  function getRequest()
	{
		return $this->getFront()->getRequest();
	}
	public function getFront()
	{
		$front = Cfw_Front::getInstance();
		return $front;
	}
	
	public function assign ($key, $value)
	{
		$controller = $this->getName();
		$this->getFront()->getRegistry()->setTemplateVar ($controller, $key, $value);
	}
	
	public function render( $action = null)
	{
		$controller = $this->getName();
		$this->getFront()->render($controller, $action);
	}
	
	public function redirect ($action = null)
	{
		$controller = $this->getName();
		$this->getFront()->redirect ($controller, $action);
	}
	
	public function getUrl ($action = null, $id = 0)
	{
		$controller = $this->getName();
		return $this->getFront()->getUrl ($controller, $action);
	}
	public function getLink($controller, $action) {
		return $this->getFront()->getUrl ($controller, $action);
	}
	
	public function action ($action= null)
	{
		$controller = $this->getName();
		$this->getFront()->action($controller ,$action);
	}
	
	public function getName ()
	{
		static $name;
		if (is_null($name)){
			$exploded = explode('_', get_class($this));
			$name = strtolower($exploded[2]);
		}
		return $name;	
	}
}